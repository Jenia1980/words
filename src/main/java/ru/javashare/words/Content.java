package ru.javashare.words;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

class Content {
    private String text;

    public Content(String text) {
        this.text = text;
    }

    public String getContent() {
        return text;
    }

    public void setContent(String text) {
        this.text = text;
    }

    public ArrayList<String> splitContent(int n) {
        String t;
        t = this.text;//.replace(" ", "");
        ArrayList<String> wordArray = new ArrayList<>();
        for (int i = 0; i < t.length()-n; i = i + n){
            wordArray.add(t.substring(i, i+n));
        }
        if (n*wordArray.size() != t.length()){
            wordArray.add(t.substring(n*wordArray.size()));
        }
        Collections.shuffle(wordArray);
        return wordArray;
    }
}
