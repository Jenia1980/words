package ru.javashare.words;

class FractWord {

    private int position;
    private String fract;

    public int getPosition() {
        return position;
    }

    public String getFract() {
        return fract;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public void setFract(String fract) {
        this.fract = fract;
    }

    public FractWord(int position, String fract) {
        this.position = position;
        this.fract = fract;
    }
}
