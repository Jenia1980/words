package ru.javashare.words;

import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.FlowPane;

import java.util.ArrayList;

public class WordsController {
    @FXML
    private FlowPane area2;

    @FXML
    private Label text;

    private String con, temp;

    @FXML
    protected void Click() {
        int n = 4;
        text.setWrapText(true);
        Content list = new Content("Обещать – дело господское, исполнять – холопское");
        con = list.getContent();
        if (area2.getChildren().size() == 0) text.setText(list.getContent().substring(0, n));
        list.setContent(list.getContent().substring(n));
        ArrayList<String> wordArr = list.splitContent(n);
        if (area2.getChildren().size() == 0) {
            for (int i = 0; i < wordArr.size(); i++) {
                Label x = new Label();
                x.setText(wordArr.get(i));
                x.setStyle("-fx-font-size: 25px; -fx-border-color: black; -fx-background-color: lime; -fx-padding: 10px;");
                area2.getChildren().add(i, x);
            }
        }
    }
    @FXML
    protected void Scale() {
        for (Node x: area2.getChildren()) {
            x.setOnMouseEntered(new EventHandler<MouseEvent>() {
                @Override public void handle(MouseEvent e) {
                    x.setScaleX(1.5);
                    x.setScaleY(1.5);
                }
            });

            x.setOnMouseClicked(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent e) {
                    Label a = new Label();
                    if (x.getClass() == Label.class){
                        a = (Label) x;
                        temp = text.getText() + a.getText();
                        if (con.contains(temp)) {
                            text.setText(temp);
                            area2.getChildren().remove(x);
                        }
                    }
                }
            });

            x.setOnMouseExited(new EventHandler<MouseEvent>() {
                @Override public void handle(MouseEvent e) {
                    x.setScaleX(1);
                    x.setScaleY(1);
                }
            });
        }
    }
}